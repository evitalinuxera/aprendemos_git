# README #

Este repositorio sirve para practicar y aprender los primeros comandos en consola

ls
cd 
ls -a

y además para ejercitarnos en el manejo de git y sus principales características

git status      Sirve para ver qué cosas cambiaron desde la última vez
git add         Sirve para agregar archivos al control de versiones
git commit      Sirve para preparar la subida. Lleva el modificador -m "texto NNN"
git push        Sirve para subir los archivos
git pull        Actualiza la copia local con lo que hay en el repositorio